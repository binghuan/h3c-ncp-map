import Vue from 'vue'
import Axios from "axios"
import loadingInterceptor from './interceptors/loading-interceptor'

import { config as projectConfig } from '@/plugins/projectConfig'


const axios = Axios.create({
    // baseURL:
    //     process.env.NODE_ENV === "development"
    //         ? projectConfig.server_development
    //         : projectConfig.server_production, // api的base_url
    baseURL: projectConfig.serverUrl,
    timeout: 50000, // request timeout
    withCredentials: false, // 设置运行跨域操作
    showModal: true, // 显示loading的配置，默认true显示，false不显示
    showError: true // 是否弹框显示ajax请求的错误信息，如果为false，就不显示
});

// 加载axios拦截器
loadingInterceptor.install(axios);
Vue.prototype.$http = axios
// 抛出接口请求方法，方便一些不依赖vue的组件调用
export const request = axios;
