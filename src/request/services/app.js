import { request } from '../index'

/**
 * 根据id获取应用
 */
export function GetApp (appId) {
  return request({
    url: APP_GET,
    method: 'get',
    params: { appId }
  })
}

export function GetYiqingData() {
  return request.get(YIQING_HTTP_API)
}
/**
 * 用户信息接口
 */
export function GetUserInfo (username = 'xuda') {
  return request({
    url: USER_INFO,
    method: 'POST',
    data: { username }
  }).then(res => res.data)
}

/**
 * 统计访问次数
 */
export function AddVisitRecord(){
  return request({
      url:'/emergency/countVisit',
      method:'get'
  })
}