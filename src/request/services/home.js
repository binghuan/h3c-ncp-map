import { request } from '../index.js'

const EPIDEMIC_CHINA = '/epidemic/china';
const EPIDEMIC_STATIC = '/epidemic/statistics';
const EPIDEMIC_PROVINCE = '/epidemic/province';
const EPIDEMIC_CITY = '/epidemic/city';
const EPIDEMIC_DETAIL = '/emergency/getDetailByCity';
const EPIDEMIC_OVERSEA = '/epidemic/oversea';

/**
 * 获取中国疫情总数据及昨日新增数据
 */
export function GetEpidemicChina() {
    return request({
        url: EPIDEMIC_CHINA,
        method: 'get',
    })
}
/**
 * 获取中国疫情折线图走势数据
 */
export function GetEpidemicStatic() {
    return request({
        url: EPIDEMIC_STATIC,
        method: 'get',
    })
}
/**
 * 获取全国省市疫情数据
 */
export function GetEpidemicProvince() {
    return request({
        url: EPIDEMIC_PROVINCE,
        method: 'get',
    })
}
/**
 * 获取全国省市疫情数据
 */
export function GetEpidemicCity(city) {
    return request({
        url: EPIDEMIC_CITY,
        method: 'get',
        params:{ city : city }
    })
}
/**
 * 获取全国省市疫情数据
 */
export function GetEpidemicDetail(province, city) {
    return request({
        url: EPIDEMIC_DETAIL,
        method: 'get',
        params:{ province: province, city : city }
    })
}
/**
 * 获取海外疫情数据
 */
export function GetEpidemicOversea() {
    return request({
        url: EPIDEMIC_OVERSEA,
        method: 'get',
    })
}