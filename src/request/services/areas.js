import { request } from '../index.js'
/**
 * 根据当前城市和选择地点的经纬度查询周边小区信息
 * @param {{lat:Number,lng:Number}}} latlng 
 * @param {string} province 
 * @param {string} city 
 */
export function GetRoundInfo(latlng, province, city) {
    var url = '/emergency/getDistanceByCity'
    return request({
        url: url,
        method: 'get',
        params: {
            province,
            city,
            lon: latlng.lng,
            lat: latlng.lat
        }
    })
}

/**
 * 根据行政区划获取选中市的确诊病例小区分布信息
 * @param {string} province 
 * @param {string} city 
 */
export function GetCityData(province, city) {
    var url = '/emergency/getDetailByCity'
    return request({
        url: url,
        method: 'get',
        params: {
            province,
            city
        }
    })
}
/**
 * 获取城市数据
 */
export function GetCityList(){
    var url = window.location.origin + '/data/city.json'
    return request({
        url:url,
        method:'get'
    })
}
/**
 * 获取城市经纬度信息
 */
export function GetCityLocation(){
    var url = window.location.origin +'/data/city-location.json'
    return request({
        url:url,
        method:'get'
    })
}

/**
 * 获取所有省的行政区划边界
 */
export function GetProvinceBounds(){
    var url = window.location.origin + '/data/provinceBounds.json'
    return request({
        url:url,
        method:'get'
    })
}
/**
 * 获取各省市的时空变化
 */
export function GetSpaceChange(){
    var url =  '/emergency/getSpaceChange'
    return request({
        url:url,
        method:'get'
    })
}