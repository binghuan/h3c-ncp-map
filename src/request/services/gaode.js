import { request } from '../index'
import GaoDe from '@/utils/gaode.js'
/**
 * 高德地图检索poi功能
 * @param {*} keywords 
 * @param {*} city 
 */
export function GaoDePOISearch(keywords, city) {
    var key = GaoDe.RandomKey()
    var types = '190400|991000'
    var url = "https://restapi.amap.com/v3/assistant/inputtips?key=" + key + "&keywords=" + keywords
    url += '&city=' + city

    return request({
        url: url,
        method: 'get',
    })
}

/**
 * 根据经纬度获取当前所在城市
 */
export function Regeo(lng, lat) {
    var key = GaoDe.RandomKey()
    var url = 'https://restapi.amap.com/v3/geocode/regeo?key=' + key + '&poitype=行政区划&radius=0&extensions=all&batch=false&roadlevel=1'

    url += '&location=' + lng + ',' + lat
    return request({
        url: url,
        method: 'get'
    })
}

/**
 * 根据poi的id检索poi详情
 * @param {*} id 
 */
 function GaoDePOIDetail(id) {
    var key = GaoDe.RandomKey()
    var url = 'https://restapi.amap.com/v3/place/detail?key=' + key + '&id=' + id
    return request({
        url: url,
        method: 'get',
    })
}
