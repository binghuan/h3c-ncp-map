import Vue from 'vue';
import App from './App.vue';
import {router} from './router';
import store from './store';
import VCharts from 'v-charts'

import '@/icons';
import Vant from 'vant';
import 'vant/lib/index.css';
import directive from '@/directive'; // 引入自定义指令
import {mockXHR} from "../mock";
//import './assets/iconfont/iconfont.css'
import {AddVisitRecord} from './request/services/app'

Vue.use(Vant);
Vue.use(VCharts);
Vue.use(directive);
Vue.config.productionTip = false;

if (window.projectConfig.isMock && process.env.NODE_ENV === 'production') {
    mockXHR()
}
// 添加访问量
AddVisitRecord()

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
