import Vue from 'vue';
import Vuex from 'vuex';

import {request} from "@/request";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    countData: {},
    drCountData:{},
    sameWayData:{},
    dayUpData:{},
      mapData:{},
  },
  mutations: {
    setData(state, data) {
      this.state.countData = data;
    },
    setdrData(state,data){
      this.state.drCountData = data
    },
    setSameWayData(state,data){
      this.state.sameWayData = data
    },
    setDayUpData(state,data){
      this.state.dayUpData = data
    },
      setMapData(state,data){
          this.state.mapData = data
      }
  },
  actions: {
    async getYIQINGdata({commit}) {
      let res = await request.get('YIQING_HTTP_API');
      await commit('setData', res.data.data)
    }
  },
  modules: {},
});
