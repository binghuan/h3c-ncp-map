import axios from 'axios';
// import Vue from 'vue';
import loadingInterceptor from "@/request/interceptors/loading-interceptor";

const Axios = axios.create({
});
// 加载axios拦截器
loadingInterceptor.install(Axios);

export async function get(url, data) {
  try {
    let res = await Axios.get(url, {
      params: data,
    });
    return new Promise((resolve) => {
      resolve(res.data);
    });
  } catch (err) {
    console.error(err);
    // Vue.prototype.$notify({
    //   showClose: true,
    //   duration: 2000, // 弹框显示时间，毫秒
    //   message: '服务器错误',
    //   type: 'error',
    // });
  }
}
export async function post(url, data) {
  try {
    let res = await Axios.post(url, JSON.stringify(data));
    return new Promise((resolve) => {
        resolve(res.data);
    });
  } catch (err) {
    console.error(err);
    // Vue.prototype.$notify({
    //   showClose: true,
    //   duration: 2000, // 弹框显示时间，毫秒
    //   message: '服务器错误',
    //   type: 'error',
    // });
  }
}
