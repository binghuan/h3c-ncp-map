import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);


const routes = [
    // {
    //     path: '*',
    //     redirect: '/main'
    // },
    {
        path: '/main',
        name:'main',
        component: () => import('@/views/main/index.vue'),
        meta: {
            title: '全国新冠肺炎疫情实时动态'
        }
    },
    {
        path: '/user',
        name:'user',
        component: () => import('@/views/user/index.vue'),
        meta: {
            title: '个人中心'
        }
    },
    {
        path: '/news',
        name:'news',
        component: () => import('@/views/news/index.vue'),
        meta: {
            title: '疫情动态'
        }
    },
    {
        path: '/fire',
        name:'fire',
        component: () => import('@/views/plague/HomeIndex.vue'),
        meta: {
            title: '疫情'
        }
    },
    {
        path: '/sameWay',
        name:'sameWay',
        component: () => import('@/views/same-way/index.vue'),
        meta: {
            title: '疫情'
        }
    },
    {
        path: '/device-api',
        name:'device-api',
        component: () => import('@/views/user/device-api.vue'),
        meta: {
            title: '原生API演示'
        }
    },
    {
        path:'/areas',
        name:'areas',
        component:()=>import('@/views/areas/index.vue'),
        meta:{
            title:'小区分布'
        }
    },
    {
        path:'/geo-evolution',
        name:'geo-evolution',
        component:()=>import('@/views/geo-evolution/index.vue'),
        meta:{
            title:'疫情演进'
        }
    },
    {
        path:'/router',
        name:'router',
        // component:()=>import('@/views/router/index.vue'),
        component:()=>import('@/views/same-way/index.vue'),
        meta:{
            title:'同程查询'
        }
    },
    {
        path:'/hotSearch',
        name:'hotSearch',
        component:()=>import('@/views/hot-search/index.vue'),
        meta:{
            title:'疫情热搜'
        }
    }
];

// add route path
routes.forEach(route => {
    route.path = route.path || '/' + (route.name || '');
});

const router = new Router({routes});

router.beforeEach((to, from, next) => {
    const title = to.meta && to.meta.title;
    if (title) {
        document.title = title;
    }
    next();
});

export {
    router
};