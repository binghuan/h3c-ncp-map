import L from 'leaflet'
import {isMobile} from '../../utils/index'
class AreaLayers {
    constructor() {
        this.areasLyerGroup = L.featureGroup()
    }
    init(map) {
        this.map = map
        var that = this
        this.areasLyerGroup.addTo(map)
        this.showTooltipLevel = 12 //点标注的文本注记一直显示的层级
        this.showTooltip = (map.getZoom() >= this.showTooltipLevel)

        // 地图缩放事件侦听
        map.on('zoomend', function (event) {
            var zoom = event.target.getZoom()
            if (zoom >= that.showTooltipLevel && !that.showTooltip) {
                that.showTooltip = !that.showTooltip
                that.areasLyerGroup.eachLayer(function (layer) {
                    layer.unbindTooltip()
                    layer.bindTooltip(layer.options.showAddress, { direction: 'top', permanent: true, className: 'areaTooltip' })
                })
            } else if (zoom < that.showTooltipLevel && that.showTooltip) {
                that.showTooltip = !that.showTooltip
                that.areasLyerGroup.eachLayer(function (layer) {
                    layer.unbindTooltip()
                    layer.bindTooltip(layer.options.showAddress, { direction: 'top', permanent: false, className: 'areaTooltip' })
                })
            }
        })
    }
    /**
     * 移除地图上的点
     */
    removePoints() {
        this.areasLyerGroup.clearLayers()
    }

    /**
     * 给地图上添加点
     * @param {Array} points 
     */
    addPoints(points) {
        try {
            var that = this
            var mobileFlag = isMobile()
            var markers = points.map(function (p) {
                var popup = L.popup().setContent('曾确诊地点：' + p.showAddress + '<br/>曾确诊人数：' + (p.cntSumCertain >= 0 ? (p.cntSumCertain + '&nbsp;人') : '暂无上报信息'))
                var className = p.cntSumCertain >= 0 ? 'areaMarker' : 'noAreaMarker'
                var icon = L.divIcon({ className: className, html: "<div>" + (p.cntSumCertain >= 0 ? p.cntSumCertain : '-') + "</div>" })

                var marker = L.marker(L.latLng(Number(p.lat), Number(p.lng)), { icon: icon, showAddress: p.showAddress, cntSumCertain: p.cntSumCertain }).bindPopup(popup)
                if (!mobileFlag) {
                    var permanent = that.map.getZoom() >= that.showTooltipLevel
                    marker.bindTooltip(p.showAddress, { direction: 'top', permanent: permanent, className: 'areaTooltip' })
                }
                return marker
            })

            markers.forEach(function (element) {
                that.areasLyerGroup.addLayer(element)
            });
        } catch (error) {
            console.error(error)
        }
    }
}

export default AreaLayers