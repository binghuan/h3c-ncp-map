import L from 'leaflet'
import chinaProvider from './leaflet.ChineseTmsProviders'

class LMap{
    constructor(){
        this.map = null
        this.initCenter = [34.773586, 110.06379]//地图初始化时的地图中心点
        this.initZoom = 5

        this.areaZoom = 13
        this.cityZoom = 9
    }
    /**
     * 加载地图
     * @param {Dom} mapDom 
     * @param {Object} mapOptions 
     * @param {{map:'mapname'},options:Object} mapType 
     */
    init(mapDom,mapOptions,mapType){
        var options = mapOptions||{} 
        this.map = L.map(mapDom, Object.assign({
            center: this.initCenter,
            zoom: this.initZoom,
            attributionControl: false,
            zoomControl:false
        },options));
        // 添加地图控件
        // this.map.addControl(L.control.zoom({position:'bottomright'}))
            
        // chinaProvider('TianDiTu.Normal.Map', { maxZoom: 18, minZoom: 5 }).addTo(this.map);
        // chinaProvider('TianDiTu.Normal.Annotion', { maxZoom: 18, minZoom: 5 }).addTo(this.map);
        if(mapType===undefined||mapType===null){
            chinaProvider('GaoDe.Normal.Map', { maxZoom: 18, minZoom: 5 }).addTo(this.map);
        }else{
            chinaProvider(mapType.map,mapType.options).addTo(this.map);
        }
        // chinaProvider('Geoq.Normal.Gray', { maxZoom: 18, minZoom: 5 }).addTo(this.map);
    }
    destory(){
        if(this.map){
            this.map.remove()
            this.map = null
        }
    }

    /**
     * 定位到小区
     * @param {Number} lat 
     * @param {Number} lng 
     */
    flyToArea(lat,lng){
        this.map.setView(L.latLng(lat,lng),this.areaZoom)
    }
    /**
     * 定位到城市
     * @param {Number} lat 
     * @param {Number} lng 
     */
    flyToCity(lat,lng){
        this.map.setView(L.latLng(lat,lng),this.cityZoom)
    }
}

export default LMap