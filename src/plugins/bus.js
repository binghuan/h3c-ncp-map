/**
 * @author xuda-21609
 * @date 2019/10/8
 * @Description: 非父子组件通信插件
 */

function Bus(Vue) {
    let bus = new Vue();

    Object.defineProperties(bus, {
        on: {
            get: function get() {
                return this.$on.bind(this)
            }
        },
        once: {
            get: function get() {
                return this.$once.bind(this)
            }
        },
        off: {
            get: function get() {
                return this.$off.bind(this)
            }
        },
        emit: {
            get: function get() {
                return this.$emit.bind(this)
            }
        }
    });

    Object.defineProperty(Vue, 'bus', {
        get: function get() {
            return bus
        }
    });

    Object.defineProperty(Vue.prototype, '$bus', {
        get: function get() {
            return bus
        }
    });
}
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(Bus)
}
export default Bus;
