try{
  const projectConfigs = (process.env.NODE_ENV === 'development' ? window.projectConfig : (window.projectConfig.production?window.projectConfig.production:{} ))

  const SYS_SETTING_KEY = 'sys-setting'
  
  var sysSetting = JSON.parse(window.localStorage.getItem(SYS_SETTING_KEY) || '{}')
  const projectConfig = Object.assign(projectConfigs, sysSetting)
  
  function ChangeFavicon(faviconUrl) {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'icon';
    link.href = faviconUrl;
    document.getElementsByTagName('head')[0].appendChild(link);
  }
  
}catch(e){
  console.error(e)
}

export default {
  vueObj: null,
  /**
   * 把全局的系统设置挂接到vue实例上，并设置系统title
   * @param Vue
   */
  install(Vue) {
    Vue.prototype.SYS_SETTING_KEY = SYS_SETTING_KEY
    Vue.prototype.$project = projectConfig
    this.vueObj = Vue
    this.setBrowser(sysSetting)
    this.updateSettingCache(sysSetting)
  },
  /**
   * 更新系统设置
   * @param setting
   */
  updateSetting(setting) {
    var assignSetting = setting
    if (Array.isArray(setting)) {
      assignSetting = {}
      setting.forEach(st => {
        assignSetting[st.configKey] = st.configValue.match(/^false$|^true$/) ? JSON.parse(st.configValue) : st.configValue
      })
    }
    this.setBrowser(assignSetting)
    this.updateSettingCache(assignSetting)
  },
  updateSettingCache(setting) {
    window.localStorage.setItem(SYS_SETTING_KEY, JSON.stringify(setting))
  },
  merageSettingCache(newSetting) {
    var sysSetting = JSON.parse(window.localStorage.getItem(SYS_SETTING_KEY) || '{}')
    var setting = Object.assign(sysSetting, newSetting)
    window.localStorage.setItem(SYS_SETTING_KEY, JSON.stringify(setting))
  },
  setBrowser(setting) {
    if (!this.vueObj) {
      console.error('请先在Vue中加载projectConfig，然后再调用updateSetting方法！')
    } else {
      this.vueObj.prototype.$project = Object.assign(this.vueObj.prototype.$project, setting)
    }
    if (document && document.title !== undefined && document.title !== null) {
      document.title = this.vueObj.prototype.$project.projectName
    }
    if (this.vueObj.prototype.$project.favicon) {
      ChangeFavicon(this.vueObj.prototype.$project.serverUrl + '/' + this.vueObj.prototype.$project.favicon)
    }
  }
}

export const config = projectConfig
