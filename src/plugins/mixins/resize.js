/**
 * @author xuda-21609
 * @date 2019/9/23
 * @Description: 窗口响应式大小
 */
export default {
    mounted() {
        window.addEventListener('resize', this.__resizeHandler);
    },
    beforeDestroy() {
        window.removeEventListener('resize', this.__resizeHandler);
    },
    methods:{
        __resizeHandler(e) {
            console.log(e)
        }
    }
}