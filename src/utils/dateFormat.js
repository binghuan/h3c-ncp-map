/**
 * 时间格式化为02.09这样的格式
 */
export function dateformat(format) {
    var d = new Date(format);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }
    var val = month + '.' + day;
    return val;
}