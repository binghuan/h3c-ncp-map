'use strict'
const path = require('path')
const projectConfig = require('./public/project.config')

function resolve(dir) {
    return path.join(__dirname, dir)
}
const name = projectConfig.projectName || '移动端webapp开发框架' //页面标题

module.exports = {
    name: name,
    resolve: {
        alias: {
            '@': resolve('src'),
            'api': resolve('src/request/services')
        }
    }
}