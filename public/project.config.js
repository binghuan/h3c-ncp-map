/**
 * 配置项为自执行工厂函数，并对各种情况做了处理
 * @author
 * @date 2020/2/10
 */
(function (root, factory) {
    if (typeof exports === 'object' && typeof module === 'object')
        module.exports = factory();
    else if (typeof define === 'function' && define.amd)
        define([], factory);
    else if (typeof exports === 'object')
        exports["projectConfig"] = factory();
    else
        root["projectConfig"] = factory();

})(this, function () {
   /* console.log("加载配置项的内容===");
    console.log(window)*/
    var globals = {}

    /**
     * 服务器端URL
     */
    globals.serverUrl = 'http://39.99.193.71:8090'//'http://xh.free.idcfengye.com' //http://39.99.193.71:8090

    /**
     * 静态资源路径；
     * @type {string}
     */
    globals.staticPath = '';
    /**
     * 页面名称
     * @type {string}
     */
    globals.projectName = '移动端webapp开发框架';
    /**
     * 是否开启mock数据
     * @type {boolean}
     */
    globals.isMock = false;
    /**
     * 开发环境后端接口地址
     * @type {string}
     */
    globals.server_development = '/dev-api';
    /**
     * 生产环境后端接口地址
     * @type {string}
     */
    globals.server_production = '';
    /**
     * 动画间隔时间
     * @type {number}
     */
    globals.animationTime = 3000;

    globals.locationCity = {
        province:'河南省',
        city:'郑州市',
        code:'410100',
        center:[34.76,113.65],//[lat,lng]"lng":"113.65","lat":"34.76"
        bounds:[[34.688007808930664, 113.55394152109513],[34.82791093991075, 113.810746941017]],//[[西南],[东北]]
        minZoom:8
    }
    return globals;
});
